python-cowboy-bike
==================

[![Gitlab pipeline status (branch)](https://img.shields.io/gitlab/pipeline/samueldumont/python-cowboy-bike/master.svg)](https://gitlab.com/samueldumont/python-cowboy-bike/pipelines)
[![coverage report](https://gitlab.com/samueldumont/python-cowboy-bike/badges/master/coverage.svg)](https://gitlab.com/samueldumont/python-cowboy-bike/badges/master)
[![PyPI - Downloads](https://img.shields.io/pypi/dm/cowboybike.svg)](https://libraries.io/pypi/cowboybike)
[![PyPI](https://img.shields.io/pypi/v/cowboybike.svg)](https://pypi.org/project/cowboybike/)
[![PyPI - Status](https://img.shields.io/pypi/status/cowboybike.svg)](https://pypi.org/project/cowboybike/)
[![Documentation Status](https://readthedocs.org/projects/cowboybike/badge/?version=master)](https://cowboybike.readthedocs.io/en/master/?badge=master)

### Disclaimer ### 

I have no affiliation with Cowboy and built this on my free time. This is the basis for a Home-Assistant plugin, which will come soon.
Feel free to open bug fixes and reports.

Documentation is available here : https://cowboybike.readthedocs.io/en/latest/