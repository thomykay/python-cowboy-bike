.. cowboybike documentation master file, created by
   sphinx-quickstart on Fri Jul 19 08:05:29 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to cowboybike's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   api-usage
   api/cowboybike

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
