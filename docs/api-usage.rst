############################
Getting started with the API
############################

cowboybike is built by reverse engineering the Cowboy app. It is unofficial and unsupported.
It may not work in the future and Cowboy could totally remove way to connect to it (not probably but we never now)

``cowboybike.Cowboy`` class
=======================

To connect to Cowboy, create a ``cowboybike.Cowboy`` object:

.. code-block:: python

   import cowboybike

   cowboy = cowboybike.Cowboy.with_auth("user@test.com","PASSWORD")

You can then use the cowboy object to query the data:

.. code-block:: python

   cowboy.refreshData()
   print(cowboy.getBike().getStateOfCharge())

There is no way to reauth as the tokens are long living.
However, after querying for tokens more than a few times, your app token could be revoked and you would have to log again.
